# Documentation

### Introduction
Console service for parsing strings containing author names. The following author names are supported for parsing:

> 1. John Doe => Firstname: John Lastname: Doe
> 2. Doe, John => Firstname: John Lastname: Doe
> 3. Hans-Christiansen Jensen => Firstname: Hans-Christiansen Lastname: Jensen
> 4. H-C Jensen => Firstname: H-C Lastname: Jensen
> 5. P.H. Kristiansen => Firstname: P.H. Lastname: Kristiansen
> 6. Kristiansen P.H. => Firstname: P.H. Lastname: Kristiansen
> 7. Peter Hans Kristiansen => Firstname: Peter Hans Lastname => Kristiansen

### Prerequsites
It is required to have Java and Maven installed locally.

### Installation
The codebase is a Maven project.

Run the command:
```sh
mvn verify
```
to build and run the tests.

Run the command:
```sh
mvn exec:java -D"exec.mainClass"="dk.radoslav.App"
```
to start the project.



### Content
The solution consists of these files:
1. `App.java` - Provides starting point of the program and a console interface to the service
2. `AuthorsService.java` - Class which will do the main work for parsing the authrors string 
3. `TestAuthorsService.java` - Various tests for the service

### Algorithm
Here are the basic steps of the algorithm:

###### 1. Classify the words
Since we know something about the name types from the example we can classify them:

1. All names ending with _","_ are last names
2. All names containing two points _2 x "."_ are first names
3. All names containing one point are _"."_ are middle names (i.e. the last part of a first name)
4. All names containing one dash _"-"_ are first names
5. If a name is not in matching the criterias above it is unknown type

###### 2. Find names with three words
After we split the string into pieces we can take the first three chunks and try to build a name as the examples provided. Most of them are for a name with thwo words, but there are examples also with three words 

###### 3. Find names with two words
If we don't succeed we try to build a name with the first two words only. the last word will be used as a first word when we do the next iteration.

###### 4. Repeat
Until we have some unused chunks (words) we can repeat 2. and 3. After each itteration store the result in a collection and return it at the end.

### Testing
In the test file are defined all the author name patterns as readonly variables. 

The following tests will combine each of these variables and execute all possibilities for two concatinated names without modification (i.e. adding of some extra commas etc.)
 