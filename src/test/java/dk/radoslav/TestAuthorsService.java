package dk.radoslav;

import java.util.Arrays;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit tests for 'AuthorsService'.
 */
public class TestAuthorsService  extends TestCase
{
	private final String unmodified_firstname_lastname_1 = "John Doe";  // 1
	private final String lastname_comma_firstname_2 = "Doe, John";  // 2
	private final String firstname_dash_secondname_lastname_3 = "Hans-Christiansen Jensen";  // 3
	private final String firstnameletter_dash_secondnameletter_lastname_4 = "H-C Jensen";  // 4
	private final String firstnameletter_point_secondnameletter_point_lastname_5 = "P.H. Kristiansen"; // 5
	private final String lastname_firstnameletter_point_secondnameletter_point_6 = "Kristiansen P.H."; // 6 
	private final String unmodified_firstname_secondname_lastname_7 = "Peter Hans Kristiansen";  // 7
	private final String firstname_secondnameletter_lastname_8 = "Peter H. Kristiansen"; // 8 
	
    public TestAuthorsService( String testName )
    {
        super( testName );
    }

    public static Test suite()
    {
        return new TestSuite( TestAuthorsService.class );
    }
    
    private static void test(String testId, String input){
    	
    	AuthorsService as = new AuthorsService();
    	String[] res = as.parse(input);
    	
    	System.out.println(testId + ":" + input + " => " + Arrays.toString(res));
    }

    /**
     * Test when the first author is name '1'
     */
    
    public void test_authors_1_1()
    {
    	System.out.println();
    	String input = unmodified_firstname_lastname_1 + " " + unmodified_firstname_lastname_1;
    	test("11", input);
    }
    
    public void test_authors_1_2()
    {
    	String input = unmodified_firstname_lastname_1 + " " + lastname_comma_firstname_2;
    	test("12", input);
    }
    
    public void test_authors_1_3()
    {
    	String input = unmodified_firstname_lastname_1 + " " + firstname_dash_secondname_lastname_3;
    	test("13", input);
    }
    
    public void test_authors_1_4()
    {
    	String input = unmodified_firstname_lastname_1 + " " + firstnameletter_dash_secondnameletter_lastname_4;
    	test("14", input);
    }
    
    public void test_authors_1_5()
    {
    	String input = unmodified_firstname_lastname_1 + " " + firstnameletter_point_secondnameletter_point_lastname_5;
    	test("15", input);
    }
    
    public void test_authors_1_6()
    {
    	String input = unmodified_firstname_lastname_1 + " " + lastname_firstnameletter_point_secondnameletter_point_6;
    	test("16", input);
    }
    
    public void test_authors_1_7()
    {
    	String input = unmodified_firstname_lastname_1 + " " + unmodified_firstname_secondname_lastname_7;
    	test("17", input);
    }
    
    public void test_authors_1_8()
    {
    	String input = unmodified_firstname_lastname_1 + " " + firstname_secondnameletter_lastname_8;
    	test("18", input);
    }

    /**
     * Test when the first author is name '2'
     */
    
    public void test_authors_2_1()
    {
    	System.out.println();
    	String input = lastname_comma_firstname_2 + " " + unmodified_firstname_lastname_1;
    	test("21", input);
    }
    
    public void test_authors_2_2()
    {
    	String input = lastname_comma_firstname_2 + " " + lastname_comma_firstname_2;
    	test("22", input);
    }
    
    public void test_authors_2_3()
    {
    	String input = lastname_comma_firstname_2 + " " + firstname_dash_secondname_lastname_3;
    	test("23", input);
    }
    
    public void test_authors_2_4()
    {
    	String input = lastname_comma_firstname_2 + " " + firstnameletter_dash_secondnameletter_lastname_4;
    	test("24", input);
    }
    
    public void test_authors_2_5()
    {
    	String input = lastname_comma_firstname_2 + " " + firstnameletter_point_secondnameletter_point_lastname_5;
    	test("25", input);
    }
    
    public void test_authors_2_6()
    {
    	String input = lastname_comma_firstname_2 + " " + lastname_firstnameletter_point_secondnameletter_point_6;
    	test("26", input);
    }
    
    public void test_authors_2_7()
    {
    	String input = lastname_comma_firstname_2 + " " + unmodified_firstname_secondname_lastname_7;
    	test("27", input);
    }
    
    public void test_authors_2_8(){
    	String input = lastname_comma_firstname_2 + " " + firstname_secondnameletter_lastname_8;
    	test("28", input);
    }
    
    /**
     * Test when the first author is name '3'
     */
    
    public void test_authors_3_1()
    {
    	System.out.println();
    	String input = firstname_dash_secondname_lastname_3 + " " + unmodified_firstname_lastname_1;
    	test("31", input);
    }
    
    public void test_authors_3_2()
    {
    	String input = firstname_dash_secondname_lastname_3 + " " + lastname_comma_firstname_2;
    	test("32", input);
    }
    
    public void test_authors_3_3()
    {
    	String input = firstname_dash_secondname_lastname_3 + " " + firstname_dash_secondname_lastname_3;
    	test("33", input);
    }
    
    public void test_authors_3_4()
    {
    	String input = firstname_dash_secondname_lastname_3 + " " + firstnameletter_dash_secondnameletter_lastname_4;
    	test("34", input);
    }
    
    public void test_authors_3_5()
    {
    	String input = firstname_dash_secondname_lastname_3 + " " + firstnameletter_point_secondnameletter_point_lastname_5;
    	test("35", input);
    }
    
    public void test_authors_3_6()
    {
    	String input = firstname_dash_secondname_lastname_3 + " " + lastname_firstnameletter_point_secondnameletter_point_6;
    	test("36", input);
    }
    
    public void test_authors_3_7()
    {
    	String input = firstname_dash_secondname_lastname_3 + " " + unmodified_firstname_secondname_lastname_7;
    	test("37", input);
    }
    
    public void test_authors_3_8(){
    	String input = firstname_dash_secondname_lastname_3 + " " + firstname_secondnameletter_lastname_8;
    	test("38", input);
    }
    
    /**
     * Test when the first author is name '4'
     */
    
    public void test_authors_4_1()
    {
    	System.out.println();
    	String input = firstnameletter_dash_secondnameletter_lastname_4 + " " + unmodified_firstname_lastname_1;
    	test("41", input);
    }
    
    public void test_authors_4_2()
    {
    	String input = firstnameletter_dash_secondnameletter_lastname_4 + " " + lastname_comma_firstname_2;
    	test("42", input);
    }
    
    public void test_authors_4_3()
    {
    	String input = firstnameletter_dash_secondnameletter_lastname_4 + " " + firstname_dash_secondname_lastname_3;
    	test("43", input);
    }
    
    public void test_authors_4_4()
    {
    	String input = firstnameletter_dash_secondnameletter_lastname_4 + " " + firstnameletter_dash_secondnameletter_lastname_4;
    	test("44", input);
    }
    
    public void test_authors_4_5()
    {
    	String input = firstnameletter_dash_secondnameletter_lastname_4 + " " + firstnameletter_point_secondnameletter_point_lastname_5;
    	test("45", input);
    }
    
    public void test_authors_4_6()
    {
    	String input = firstnameletter_dash_secondnameletter_lastname_4 + " " + lastname_firstnameletter_point_secondnameletter_point_6;
    	test("46", input);
    }
    
    public void test_authors_4_7()
    {
    	String input = firstnameletter_dash_secondnameletter_lastname_4 + " " + unmodified_firstname_secondname_lastname_7;
    	test("47", input);
    }
    
    public void test_authors_4_8(){
    	String input = firstnameletter_dash_secondnameletter_lastname_4 + " " + firstname_secondnameletter_lastname_8;
    	test("48", input);
    }
    
    /**
     * Test when the first author is name '5'
     */
    
    public void test_authors_5_1()
    {
    	System.out.println();
    	String input = firstnameletter_point_secondnameletter_point_lastname_5 + " " + unmodified_firstname_lastname_1;
    	test("51", input);
    }
    
    public void test_authors_5_2()
    {
    	String input = firstnameletter_point_secondnameletter_point_lastname_5 + " " + lastname_comma_firstname_2;
    	test("52", input);
    }
    
    public void test_authors_5_3()
    {
    	String input = firstnameletter_point_secondnameletter_point_lastname_5 + " " + firstname_dash_secondname_lastname_3;
    	test("53", input);
    }
    
    public void test_authors_5_4()
    {
    	String input = firstnameletter_point_secondnameletter_point_lastname_5 + " " + firstnameletter_dash_secondnameletter_lastname_4;
    	test("54", input);
    }
    
    public void test_authors_5_5()
    {
    	String input = firstnameletter_point_secondnameletter_point_lastname_5 + " " + firstnameletter_point_secondnameletter_point_lastname_5;
    	test("55", input);
    }
    
    public void test_authors_5_6()
    {
    	String input = firstnameletter_point_secondnameletter_point_lastname_5 + " " + lastname_firstnameletter_point_secondnameletter_point_6;
    	test("56", input);
    }
    
    public void test_authors_5_7()
    {
    	String input = firstnameletter_point_secondnameletter_point_lastname_5 + " " + unmodified_firstname_secondname_lastname_7;
    	test("57", input);
    }
    
    public void test_authors_5_8(){
    	String input = firstnameletter_point_secondnameletter_point_lastname_5 + " " + firstname_secondnameletter_lastname_8;
    	test("58", input);
    }
    
    /**
     * Test when the first author is name '6'
     */
    
    public void test_authors_6_1()
    {
    	System.out.println();
    	String input = lastname_firstnameletter_point_secondnameletter_point_6 + " " + unmodified_firstname_lastname_1;
    	test("61", input);
    }
    
    public void test_authors_6_2()
    {
    	String input = lastname_firstnameletter_point_secondnameletter_point_6 + " " + lastname_comma_firstname_2;
    	test("62", input);
    }
    
    public void test_authors_6_3()
    {
    	String input = lastname_firstnameletter_point_secondnameletter_point_6 + " " + firstname_dash_secondname_lastname_3;
    	test("63", input);
    }
    
    public void test_authors_6_4()
    {
    	String input = lastname_firstnameletter_point_secondnameletter_point_6 + " " + firstnameletter_dash_secondnameletter_lastname_4;
    	test("64", input);
    }
    
    public void test_authors_6_5()
    {
    	String input = lastname_firstnameletter_point_secondnameletter_point_6 + " " + firstnameletter_point_secondnameletter_point_lastname_5;
    	test("65", input);
    }
    
    public void test_authors_6_6()
    {
    	String input = lastname_firstnameletter_point_secondnameletter_point_6 + " " + lastname_firstnameletter_point_secondnameletter_point_6;
    	test("66", input);
    }
    
    public void test_authors_6_7()
    {
    	String input = lastname_firstnameletter_point_secondnameletter_point_6 + " " + unmodified_firstname_secondname_lastname_7;
    	test("67", input);
    }
    
    public void test_authors_6_8(){
    	String input = lastname_firstnameletter_point_secondnameletter_point_6 + " " + firstname_secondnameletter_lastname_8;
    	test("68", input);
    }
    
    /**
     * Test when the first author is name '7'
     */
    
    public void test_authors_7_1()
    {
    	System.out.println();
    	String input = unmodified_firstname_secondname_lastname_7 + " " + unmodified_firstname_lastname_1;
    	test("71", input);
    }
    
    public void test_authors_7_2()
    {
    	String input = unmodified_firstname_secondname_lastname_7 + " " + lastname_comma_firstname_2;
    	test("72", input);
    }
    
    public void test_authors_7_3()
    {
    	String input = unmodified_firstname_secondname_lastname_7 + " " + firstname_dash_secondname_lastname_3;
    	test("73", input);
    }
    
    public void test_authors_7_4()
    {
    	String input = unmodified_firstname_secondname_lastname_7 + " " + firstnameletter_dash_secondnameletter_lastname_4;
    	test("74", input);
    }
    
    public void test_authors_7_5()
    {
    	String input = unmodified_firstname_secondname_lastname_7 + " " + firstnameletter_point_secondnameletter_point_lastname_5;
    	test("75", input);
    }
    
    public void test_authors_7_6()
    {
    	String input = unmodified_firstname_secondname_lastname_7 + " " + lastname_firstnameletter_point_secondnameletter_point_6;
    	test("76", input);
    }
    
    public void test_authors_7_7()
    {
    	String input = unmodified_firstname_secondname_lastname_7 + " " + unmodified_firstname_secondname_lastname_7;
    	test("77", input);
    }
    
    public void test_authors_7_8(){
    	String input = unmodified_firstname_secondname_lastname_7 + " " + firstname_secondnameletter_lastname_8;
    	test("78", input);
    }
    
    /**
     * Test when the first author is name '8'
     */
    
    public void test_authors_8_1()
    {
    	System.out.println();
    	String input = firstname_secondnameletter_lastname_8 + " " + unmodified_firstname_lastname_1;
    	test("81", input);
    }
    
    public void test_authors_8_2()
    {
    	String input = firstname_secondnameletter_lastname_8 + " " + lastname_comma_firstname_2;
    	test("82", input);
    }
    
    public void test_authors_8_3()
    {
    	String input = firstname_secondnameletter_lastname_8 + " " + firstname_dash_secondname_lastname_3;
    	test("83", input);
    }
    
    public void test_authors_8_4()
    {
    	String input = firstname_secondnameletter_lastname_8 + " " + firstnameletter_dash_secondnameletter_lastname_4;
    	test("84", input);
    }
    
    public void test_authors_8_5()
    {
    	String input = firstname_secondnameletter_lastname_8 + " " + firstnameletter_point_secondnameletter_point_lastname_5;
    	test("85", input);
    }
    
    public void test_authors_8_6()
    {
    	String input = firstname_secondnameletter_lastname_8 + " " + lastname_firstnameletter_point_secondnameletter_point_6;
    	test("86", input);
    }
    
    public void test_authors_8_7(){
    	String input = firstname_secondnameletter_lastname_8 + " " + unmodified_firstname_secondname_lastname_7;
    	test("87", input);
    }
    
    public void test_authors_8_8(){
    	String input = firstname_secondnameletter_lastname_8 + " " + firstname_secondnameletter_lastname_8;
    	test("88", input);
    }
}
