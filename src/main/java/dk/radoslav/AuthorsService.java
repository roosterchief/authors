package dk.radoslav;

import java.util.Arrays;
import java.util.LinkedList;

/**
 * Class which main purpose is to parse string of authors 
 * to an array of string authors formatted in configurable format.
 * 
 * @author radoslav
 */
public class AuthorsService {

	/**
	 * The format of the returned string items from the 'parse(input)' method.
	 */
	private final String AUTHOR_ITEM = "Firstname: %s Lastname: %s";
	
	/**
	 * Converts input string of authors to array items
	 * in the format specified in 'AUTHOR_ITEM'.
	 * @param input
	 * @return
	 */
	public String[] parse(String input) {
		
		LinkedList<String> result = new LinkedList<>();

		String[] allChunks = input.trim().split(" ");
		LinkedList<String> chunksQueue = new LinkedList<>(Arrays.asList(allChunks));

		String chunk1 = "";
		String chunk2 = "";
		String chunk3 = "";
		String excessiveChunk = "";

		while (chunksQueue.size() > 0) {
			
			if (excessiveChunk != null && !excessiveChunk.equals("")) {
				chunk1 = excessiveChunk;
			} else {
				chunk1 = chunksQueue.pollFirst();
			}
			chunk2 = chunksQueue.pollFirst();
			chunk3 = chunksQueue.pollFirst();

			NameResult res = determinateName(chunk1, chunk2, chunk3);
			
			if (res != null) {
				
				result.add(String.format(AUTHOR_ITEM, res.name.firstName, 
						res.name.lastName.endsWith(",") ? res.name.lastName.substring(0, res.name.lastName.length()-1) : res.name.lastName));
						
				excessiveChunk = res.excess;
			}
			else{
				System.out.print("We could not generate a name and we will stop here... ");
				break;
			}
		}

		String[] outputResult = result.toArray(new String[result.size()]);
		return outputResult;
	}

	/**
	 * Tries to analyze three strings for author name.
	 * @param s1
	 * @param s2
	 * @param s3
	 * @return
	 */
	private NameResult determinateName(String s1, String s2, String s3) {

		if (s1 == null || s1.equals("") || s2 == null || s2.equals("")) {
			return null;
		} 
		
		else if (s3 == null || s3.equals("")) {
			NameResult res = determinateName(s1, s2);
			
			if(res!= null){
				res.excess = null;
			}
			
			return res;
		} 
		
		else if (determinateChunk(s1) == ChunkType.UnknownName
				&& determinateChunk(s2) == ChunkType.UnknownName
				&& determinateChunk(s3) == ChunkType.LastName) {

			Name name = new Name();
			name.firstName = s1 + " " + s2;
			name.lastName = s3;
			NameResult res = new NameResult();
			res.name = name;
			res.excess = null;

			return res;
		} 
		
		else if (determinateChunk(s1) == ChunkType.UnknownName
				&& determinateChunk(s2) == ChunkType.UnknownName
				&& determinateChunk(s3) == ChunkType.UnknownName) {

			Name name = new Name();
			name.firstName = s1 + " " + s2;
			name.lastName = s3;
			NameResult res = new NameResult();
			res.name = name;
			res.excess = null;

			return res;
		} 
		
		else if (determinateChunk(s1) == ChunkType.UnknownName
				&& determinateChunk(s2) == ChunkType.MiddleName
				&& determinateChunk(s3) == ChunkType.UnknownName) {

			Name name = new Name();
			name.firstName = s1 + " " + s2;
			name.lastName = s3;
			NameResult res = new NameResult();
			res.name = name;
			res.excess = null;

			return res;
		}
		
		else{
			
			NameResult res = determinateName(s1, s2);
			res.excess = s3;
			
			return res;
		}
	}
	
	/**
	 * Tries to analyze two strings for author name.
	 * @param s1
	 * @param s2
	 * @return
	 */
	private NameResult determinateName(String s1, String s2){
		if (s1 == null || s1.equals("") || s2 == null || s2.equals("")) {
			return null;
		} 
		
		else if (determinateChunk(s1) == ChunkType.UnknownName
				&& determinateChunk(s2) == ChunkType.UnknownName) {
					Name name = new Name();
					name.firstName = s1;
					name.lastName = s2;
					NameResult res = new NameResult();
					res.name = name;
					res.excess = null;

					return res;
		}
		
		else if (determinateChunk(s1) == ChunkType.LastName
				&& determinateChunk(s2) == ChunkType.UnknownName) {
					Name name = new Name();
					name.firstName = s2;
					name.lastName = s1;
					NameResult res = new NameResult();
					res.name = name;
					res.excess = null;

					return res;
		}
		
		else if (determinateChunk(s1) == ChunkType.FirstName
				&& determinateChunk(s2) == ChunkType.UnknownName) {
					Name name = new Name();
					name.firstName = s1;
					name.lastName = s2;
					NameResult res = new NameResult();
					res.name = name;
					res.excess = null;

					return res;
		}
		
		else if (determinateChunk(s1) == ChunkType.LastName
				&& determinateChunk(s2) == ChunkType.FirstName) {
					Name name = new Name();
					name.firstName = s2;
					name.lastName = s1;
					NameResult res = new NameResult();
					res.name = name;
					res.excess = null;

					return res;
		}
		
		else if (determinateChunk(s1) == ChunkType.UnknownName
				&& determinateChunk(s2) == ChunkType.LastName) {
					Name name = new Name();
					name.firstName = s1;
					name.lastName = s2;
					NameResult res = new NameResult();
					res.name = name;
					res.excess = null;

					return res;
		}
		
		else if (determinateChunk(s1) == ChunkType.FirstName
				&& determinateChunk(s2) == ChunkType.LastName) {
					Name name = new Name();
					name.firstName = s1;
					name.lastName = s2;
					NameResult res = new NameResult();
					res.name = name;
					res.excess = null;

					return res;
		}
		
		else if (determinateChunk(s1) == ChunkType.UnknownName
				&& determinateChunk(s2) == ChunkType.FirstName) {
					Name name = new Name();
					name.firstName = s2;
					name.lastName = s1;
					NameResult res = new NameResult();
					res.name = name;
					res.excess = null;

					return res;
		}
		else if (determinateChunk(s1) == ChunkType.MiddleName 
				|| determinateChunk(s2) == ChunkType.MiddleName ) {
					return null;
		}
		else{
			// We have one of these not possible scenarios: 
			// - LastName LastName
			// - FirstName FirstName
			// - MddleName MiddleName
			// - or something else not compatible to build a name...
			
			return null;
		}
	}

	/**
	 * Checks the type of a name based on known author rules.
	 * If the result cannot be recognized the type is 'Unknown'.
	 * @param s
	 * @return
	 */
	private ChunkType determinateChunk(String s) {
		if (s.indexOf(".") != s.lastIndexOf(".") || s.contains("-")) {
			return ChunkType.FirstName;
		} else if (s.contains(".")) {
			return ChunkType.MiddleName;
		} else if (s.endsWith(",")) {
			return ChunkType.LastName;
		} else {
			return ChunkType.UnknownName;
		}
	}

	/**
	 * Enum for type of author name.
	 * @author radoslav
	 *
	 */
	private enum ChunkType {
		FirstName, MiddleName, LastName, UnknownName, Invalid,
	}

	/**
	 * Author name data class.
	 */
	private class Name {
		public String firstName;
		public String lastName;
	}

	/**
	 * Class for results from author name analyzes.
	 */
	private class NameResult {
		public Name name;
		public String excess;
	}
}


