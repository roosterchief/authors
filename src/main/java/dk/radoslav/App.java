package dk.radoslav;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Console interface for the 'AuthorsService'
 * 
 * @author radoslav
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Welcome to the Authors service!\n"
        				+ "Type string containing authors or 'x' for exit: " );
        
        Scanner sc = new Scanner(System.in);
        while(true){
	        
	        String input = sc.nextLine();
	        if (input.equals("x") || input.equals("X")){
	        	break;
	        }else{
	        	AuthorsService as = new AuthorsService();
	        	String[] res = as.parse(input);
	        	System.out.println("The result is:" + Arrays.toString(res));
	        }
        }
        
        System.out.println("Bye!");
        sc.close();
    }
}
